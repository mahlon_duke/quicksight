// Performs the initial load of the dashboard, and its default options
function loadInitialDashboard() {
  console.log("loadInitialDashboard function called")

  // Select the dashboard container
  var dashboardContainer = document.getElementById("dashboardContainer");

  // Options array for customizing the dashboard's content
  var options = {
    url: "https://us-west-2.quicksight.aws.amazon.com/sn/dashboards/3a74c34e-27ea-4428-897b-3ebde32cc466",
    container: document.getElementById("dashboardContainer"),
    parameters: {
      acceloDeployment: 3806
    },
    scrolling: "no",
    height: "700px",
    width: "1000px",
    locale: "en-US"
  };

  // Load the dashboard
  var dashboard = QuickSightEmbedding.embedDashboard(options);

  // Log the dashboard object for troubleshooting
  console.log(dashboard);
};
